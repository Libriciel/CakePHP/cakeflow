<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
* Name app
* Set the name application
*/
//Configure::write('Cakeflow.app', 'name_app');

if (!defined('CAKEFLOW_DIR')) {
    define('CAKEFLOW_DIR', dirname(__FILE__).DS.'..'.DS);
}

/**
* Routing Prefix
* Set the prefix you would like to restrict the plugin to
* @see Configure::read('Routing.prefixes')
*/
//Configure::write('Routing.prefixes', array('admin', 'manager'));

/**
* Définition des paramètres communs de configuration
* cakeflow.User : classe des utilisateurs de l'application
* CAKEFLOW_xxx_MODEL : permet de faire le lien entre le plugin et les classes des différents modèles. Ex: 'User', 'Utilisateur', ...
* @var string
*/
//Configure::write('Cakeflow.user.model', 'User');

/**
* CAKEFLOW_xxx_FIELDS : permet de spécifier les champs séparés par des virgule, utilisés pour afficher les occurences du modèle lié. Ex : 'first_name,name,username'
* @deprecated
* @var string
*/
//Configure::write('Cakeflow.user.fields', 'lastname,name,username');

/**
* CAKEFLOW_xxx_FORMAT : permet spécifier le format d'affichage des occurences du model lié. Ex : '%s %s - %s'
* @deprecated
* @var string
*/
//Configure::write('Cakeflow.user.format', '%s %s (%s)');

/**
* CAKEFLOW_USER_IDSESSION : lorsqu'il est renseigné, permet spécifier la variable de session de l'id de l'utilisateur connecté. Ex : 'Auth.user.id'
* @var string
*/
//Configure::write('Cakeflow.user.idsession', 'Auth.user.id');

/**
* cakeflow.Trigger : classe des déclencheurs
* Fichier de configuration du plugin CakeFlow
* il exite trois classes qui entrent en jeux dans l'utilisation du plugin
* - la classe des utilisateurs qui vont ajouter, modifier et supprimer la définition des circuits de traitement
* - la classe des déclencheurs qui vont exécuter le traitement du circuit de traitement (valider, refuser, renvoyer à l'étape précédente, ...)
* - la classe des cibles qui sont les objets insérés dans un circuit de traitement
* Définition des paramètres particuliers à chaque classe
*    CAKEFLOW_TRIGGER_TITLE : libellé utilisé dans les listes et formulaire pour désigner les déclencheurs
*    CAKEFLOW_TRIGGER_CONDITIONS : lorsqu'il est renseigné, permet spécifier les conditions pour aller lire la liste des déclencheurs formaté comme suit : 'field1,condition1;field2,condition2'. Ex : 'actif,true;id >,10'
*    CAKEFLOW_TARGET_TITLE : libellé utilisé dans les listes et formulaire pour désigner les cibles. Ex : 'Délibérations'
*/
//Configure::write('Cakeflow.trigger.model', 'User');

/**
 * @deprecated
* @var bool Manage parapheur signature
*/
//Configure::write('Cakeflow.trigger.fields', 'nom,prenom');

/**
 * @deprecated
* @var bool Manage parapheur signature
*/
//Configure::write('Cakeflow.trigger.format', '%s %s';

/**
* @var string Trigger title
*/
//Configure::write('Cakeflow.trigger.title', 'Utilisateur');

/**
* @deprecated
* @var bool Manage parapheur signature
*/
//Configure::write('Cakeflow.trigger.conditions', '');


/**
* cakeflow.Target : classe des cibles
* @var string Manage parapheur signature
*/
//Configure::write('Cakeflow.target.model', 'Model');

/**
* @deprecated
* @var bool Manage parapheur signature
*/
//Configure::write('Cakeflow.target.fields', 'libelle');

/**
* @deprecated
* @var bool Manage parapheur signature
*/
//Configure::write('Cakeflow.target.format', '%s');
/**
* @deprecated
* @var bool Manage parapheur signature
*/
//Configure::write('Cakeflow.target.title', 'Délibération');

/**
* @var bool Manage parapheur signature
*/
//Configure::write('Cakeflow.manage.parapheur', false);

/**
* @var bool Manage circuit default
*/
//Configure::write('Cakeflow.manage.default', false);

/**
* @var string liste of type actions of treatment
*/
//Configure::write('Cakeflow.actionsOutOfTreatment', 'ST,JS,IN');

/**
* @var array types liste des types des étapes
*/
//Configure::write("Cakeflow.etapesType", [
//  'simple'=> ['value' => 1, 'label' => __('Simple')],
//  'concurrent'=> ['value' => 2, 'label' => __('Concurrent [OU]')],
//  'collaboratif'=> ['value' => 3, 'label' => __('Collaboratif [ET]')]]);

/**
* Association du trigger -1 au parapheur
* @var int
*/
//Configure::write('Cakeflow.trigger.parapheur', -1);

$versionFile = file(CAKEFLOW_DIR . DS . 'VERSION.txt');
Configure::write("Cakeflow.version", trim(array_pop($versionFile)));

define('CAKEFLOW_USER_MODEL', Configure::read('Cakeflow.user.model'));
define('CAKEFLOW_TRIGGER_MODEL', Configure::read('Cakeflow.trigger.model'));
define('CAKEFLOW_TARGET_MODEL', Configure::read('Cakeflow.target.model'));
