<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class CakeflowSchema extends CakeSchema
{
    public function before($event = [])
    {
        return true;
    }

    public function after($event = [])
    {
    }

    public $wkf_circuits = [
        'id' => ['type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'],
        'nom' => ['type' => 'string', 'null' => false, 'default' => null, 'length' => 250],
        'description' => ['type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824],
        'actif' => ['type' => 'boolean', 'null' => false, 'default' => true],
        'defaut' => ['type' => 'boolean', 'null' => false, 'default' => false],
        'created_user_id' => ['type' => 'integer', 'null' => true, 'default' => null],
        'modified_user_id' => ['type' => 'integer', 'null' => true, 'default' => null],
        'created' => ['type' => 'datetime', 'null' => true, 'default' => null],
        'modified' => ['type' => 'datetime', 'null' => true, 'default' => null],
        'indexes' => [
            'PRIMARY' => ['unique' => true, 'column' => 'id'],
            'created_user_id' => ['unique' => false, 'column' => 'created_user_id'],
            'modified_user_id' => ['unique' => false, 'column' => 'modified_user_id']
        ],
        'tableParameters' => []
    ];

    public $wkf_compositions = [
        'id' => ['type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'],
        'etape_id' => ['type' => 'integer', 'null' => false, 'default' => null],
        'type_validation' => ['type' => 'string', 'null' => false, 'default' => null, 'length' => 1],
        'trigger_id' => ['type' => 'integer', 'null' => true, 'default' => null],
        'created_user_id' => ['type' => 'integer', 'null' => true, 'default' => null],
        'modified_user_id' => ['type' => 'integer', 'null' => true, 'default' => null],
        'created' => ['type' => 'datetime', 'null' => true, 'default' => null],
        'modified' => ['type' => 'datetime', 'null' => true, 'default' => null],
        'soustype' => ['type' => 'integer', 'null' => true, 'default' => null],
        'type_composition' => ['type' => 'string', 'null' => true, 'default' => 'USER', 'length' => 20],
        'indexes' => [
            'PRIMARY' => ['unique' => true, 'column' => 'id'],
            'etape_id' => ['unique' => false, 'column' => 'etape_id'],
            'trigger' => ['unique' => false, 'column' => 'trigger_id']
        ],
        'tableParameters' => []
    ];

    public $wkf_etapes = [
        'id' => ['type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'],
        'circuit_id' => ['type' => 'integer', 'null' => false, 'default' => null],
        'nom' => ['type' => 'string', 'null' => false, 'default' => null, 'length' => 250],
        'description' => ['type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824],
        'type' => ['type' => 'integer', 'null' => false, 'default' => null],
        'ordre' => ['type' => 'integer', 'null' => false, 'default' => null],
        'created_user_id' => ['type' => 'integer', 'null' => false, 'default' => null],
        'modified_user_id' => ['type' => 'integer', 'null' => true, 'default' => null],
        'created' => ['type' => 'datetime', 'null' => true, 'default' => null],
        'modified' => ['type' => 'datetime', 'null' => true, 'default' => null],
        'soustype' => ['type' => 'integer', 'null' => true, 'default' => null],
        'cpt_retard' => ['type' => 'integer', 'null' => true, 'default' => null],
        'indexes' => [
            'PRIMARY' => ['unique' => true, 'column' => 'id'],
            'circuit_id' => ['unique' => false, 'column' => 'circuit_id'],
            'nom' => ['unique' => false, 'column' => 'nom']
        ],
        'tableParameters' => []
    ];

    public $wkf_signatures = [
        'id' => ['type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'],
        'type_signature' => ['type' => 'string', 'null' => false, 'default' => null, 'length' => 100],
        'signature' => ['type' => 'text', 'null' => false, 'default' => null, 'length' => 1073741824],
        'visa_id' => ['type' => 'integer', 'null' => true, 'default' => null],
        'indexes' => [
            'PRIMARY' => ['unique' => true, 'column' => 'id']
        ],
        'tableParameters' => []
    ];

    public $wkf_traitements = [
        'id' => ['type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'],
        'circuit_id' => ['type' => 'integer', 'null' => false, 'default' => null],
        'target_id' => ['type' => 'integer', 'null' => false, 'default' => null],
        'numero_traitement' => ['type' => 'integer', 'null' => false, 'default' => '1'],
        'treated_orig' => ['type' => 'smallinteger', 'null' => false, 'default' => '0'],
        'created_user_id' => ['type' => 'integer', 'null' => true, 'default' => null],
        'modified_user_id' => ['type' => 'integer', 'null' => true, 'default' => null],
        'created' => ['type' => 'datetime', 'null' => true, 'default' => null],
        'modified' => ['type' => 'datetime', 'null' => true, 'default' => null],
        'treated' => ['type' => 'boolean', 'null' => true, 'default' => null],
        'indexes' => [
            'PRIMARY' => ['unique' => true, 'column' => 'id'],
            'circuits' => ['unique' => false, 'column' => 'circuit_id'],
            'target' => ['unique' => false, 'column' => 'target_id'],
            'traitements_treated' => ['unique' => false, 'column' => 'treated_orig']
        ],
        'tableParameters' => []
    ];

    public $wkf_visas = [
        'id' => ['type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'],
        'traitement_id' => ['type' => 'integer', 'null' => false, 'default' => null],
        'trigger_id' => ['type' => 'integer', 'null' => false, 'default' => null],
        'signature_id' => ['type' => 'integer', 'null' => true, 'default' => null],
        'etape_nom' => ['type' => 'string', 'null' => true, 'default' => null, 'length' => 250],
        'etape_type' => ['type' => 'integer', 'null' => false, 'default' => null],
        'action' => ['type' => 'string', 'null' => false, 'default' => null, 'length' => 2],
        'commentaire' => ['type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824],
        'date' => ['type' => 'datetime', 'null' => true, 'default' => null],
        'numero_traitement' => ['type' => 'integer', 'null' => false, 'default' => null],
        'type_validation' => ['type' => 'string', 'null' => false, 'default' => null, 'length' => 1],
        'etape_id' => ['type' => 'integer', 'null' => true, 'default' => null],
        'date_retard' => ['type' => 'datetime', 'null' => true, 'default' => null],
        'indexes' => [
            'PRIMARY' => ['unique' => true, 'column' => 'id'],
            'wkf_visas_traitements' => ['unique' => false, 'column' => 'traitement_id'],
            'wkf_visas_trigger_id' => ['unique' => false, 'column' => 'trigger_id']
        ],
        'tableParameters' => []
    ];
}
