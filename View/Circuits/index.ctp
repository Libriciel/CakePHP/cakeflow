<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Utilisateurs'));
$this->Html->addCrumb(__('Circuits'));

echo $this->Html->css("Cakeflow.design.css");
echo $this->Html->tag('h3', __('Liste des circuits', true) . ' (' . $this->Paginator->counter('{:count}') . ')');
echo $this->Html2->btnCreate(__('Créer un circuit'), null, ['action' => 'add']);

echo $this->element('filtre');

// initialisation de l'entete du tableau
$tableHeaders = [
    ['title' => $this->Paginator->sort(__('nom', true), 'Nom')],
    ['title' => __('Description', true)],
    ['title' => __('Etapes', true)],
    ['title' => $this->Paginator->sort(__('actif', true), 'Actif')]];
if (Configure::read('Cakeflow.manage.default')) {
    $tableHeaders[] = ['title' => __('Défaut', true)];
}

$tableHeaders[] = ['title' => __('Actions', true)];
echo $this->Bs->table(
    $tableHeaders,
    ['striped']
);
foreach ($this->data as $rownum => $rowElement) {
    echo $this->Bs->cell($rowElement['Circuit']['nom']);
    echo $this->Bs->cell($rowElement['Circuit']['description']);
    $etapes = '';
    foreach ($rowElement['Etape'] as $etape) {
        $etapes .= $etape['Etape']['nom'] . ' (' . $etape['Etape']['label'] . ')' . '<br/>';
    }
    echo $this->Bs->cell($etapes);
    echo $this->Bs->cell($rowElement['Circuit']['actifLibelle']);
    if (Configure::read('Cakeflow.manage.default')) {
        echo $this->Bs->cell($rowElement['Circuit']['defautLibelle']);
    }

    $boutons = $this->Bs->div('btn-group');
    $boutons .= $this->Bs->btn($this->Bs->icon('eye'), [
        'action' => 'view', $rowElement['Circuit']['id']], ['type' => 'default',
        'title' => __('Visualiser le circuit'),
        'escapeTitle' => false,
        'class' => !$rowElement['ListeActions']['view'] ? 'disabled' : ''
      ]);
    $boutons .= $this->Bs->btn($this->Bs->icon('pencil-alt'), [
        'action' => 'edit', $rowElement['Circuit']['id']], ['type' => 'primary',
        'title' => __('Modifier le circuit'),
        'escapeTitle' => false,
        'class' => !$rowElement['ListeActions']['edit'] ? 'disabled' : ''
      ]);
    $boutons .= $this->Bs->btn($this->Bs->icon('list'), [
        'controller' => 'etapes', 'action' => 'index', $rowElement['Circuit']['id']], [
        'type' => 'primary',
        'escapeTitle' => false,
        'title' => __('Visualiser les étapes')
      ]);
    $boutons .= $this->Bs->btn($this->Bs->icon('copy'), [
          'action' => 'duplicate', $rowElement['Circuit']['id']], ['type' => 'primary',
          'title' => __('Dupliquer le circuit'),
          'escapeTitle' => false,
          'confirm' => __('Êtes vous sur de vouloir dupliquer le circuit "%s" ?', $rowElement['Circuit']['nom'])
        ]);
    //$boutons .= $this->Bs->btn(null,array('action' => 'visuCircuit', $rowElement['Circuit']['id']), array('type' => 'info', 'icon' => 'glyphicon glyphicon-list-alt',array('title' =>  __('Visionner', true),'class' => !$rowElement['ListeActions']['visuCircuit'] ? 'disabled' : ''  )));
    $boutons .= $this->Bs->btn($this->Bs->icon('trash'), ['action' => 'delete', $rowElement['Circuit']['id']], [
        'type' => 'danger',
        'title' => __('Supprimer le circuit'),
        'escapeTitle' => false,
        'class' => !$rowElement['ListeActions']['delete'] ? 'disabled' : '',
        'confirm' => __('Êtes vous sur de vouloir supprimer le circuit "%s" ?', $rowElement['Circuit']['nom'])
      ]);
    echo $this->Bs->cell($boutons, 'text-nowrap');
}
echo $this->Bs->endTable();

echo $this->element('paginator', ['paginator' => $this->Paginator]);
