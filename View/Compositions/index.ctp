<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Utilisateurs'));
$this->Html->addCrumb(__('Circuits'), ['controller' => 'circuits', 'action' => 'index']);
echo $this->Html->css("Cakeflow.design.css");
if (!empty($etape)) {
    $this->pageTitle = sprintf("Composition de l'étape '%s' du circuit '%s'", $etape['Etape']['nom'], $etape['Circuit']['nom']);
} else {
    $this->pageTitle = __('Composition', true);
}
$this->Html->addCrumb(__('Étapes du circuit'), ['controller' => 'etapes', 'action' => 'index', $etape['Circuit']['id']]);
$this->Html->addCrumb(__('Composition de l\'étape'));
echo $this->Html->tag('h3', $this->pageTitle);
if (!empty($compositions)) {
    $this->Paginator->options(['url' => $this->passedArgs]);
    $cells = '';
    echo $this->Bs->table(
            [
        ['title' => Configure::read('Cakeflow.trigger.title')],
        ['title' => __('Type de validation', true)],
        ['title' => __('Actions', true)]
            ],
        ['striped']
    );
    foreach ($compositions as $rownum => $composition) {
        $rows = Set::extract($composition, 'Composition');
        $triggerLibelle = $this->Bs->cell($rows['triggerLibelle']);
        if (!Configure::read('USE_PARAPHEUR') && $composition['Composition']['trigger_id'] == -1) {
            $triggerLibelle = $this->Bs->cell("<span style='cursor: help; border-bottom-color: #999; border-bottom-style: dotted; border-bottom-width: 1px;' title='Attention : Cette délégation peut poser problème. \nSolution : Activer le parapheur dans les connecteurs ou modifier/supprimer la composition'>
                <i class='fa fa-warning'></i> " . $rows['triggerLibelle'] . "</span>");
        }
        echo $triggerLibelle;
        echo $this->Bs->cell($rows['typeValidationLibelle']);
        $boutons = $this->Bs->div('btn-group');
        $boutons .= $this->Bs->btn($this->Bs->icon('eye'), ['action' => 'view', $composition['Composition']['id']], [
            'type' => 'default',
            'escapeTitle' => false,
            'title' => __('Visualiser')]);
        $boutons .= $this->Bs->btn($this->Bs->icon('pencil-alt'), ['action' => 'edit', $composition['Composition']['id']], [
            'type' => 'primary',
            'escapeTitle' => false,
            'title' => __('Modifier')]);
        $boutons .= $this->Bs->btn($this->Bs->icon('trash'), ['action' => 'delete', $composition['Composition']['id']], [
            'type' => 'danger',
            'escapeTitle' => false,
            'title' => __('Supprimer'),
            'confirm' => __('Êtes vous sur de vouloir supprimer ') . $rows['triggerLibelle'] . ' ?']);
        echo $this->Bs->cell($boutons, 'text-nowrap');
    }
    echo $this->Bs->endTable();
    //paginate
    echo $this->element('paginator', array('paginator' => $this->Paginator));
    echo $this->Bs->div('actions btn-group col-md-offset-0', null) .
    $this->Html2->btnCancel($previous);
    if ($canAdd) {
        echo $this->Html2->btnAdd(__('Ajouter une composition'), null, ['action' => 'add', $etape_id]);
    }
    echo $this->Bs->close();
}
