<?php
echo $this->Html->css("Cakeflow.design.css");

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Utilisateurs'));
$this->Html->addCrumb(__('Circuits'), ['controller' => 'circuits', 'action' => 'index']);
$this->Html->addCrumb(__('Étapes du circuit'), ['controller' => 'etapes', 'action' => 'index', $etape['Circuit']['id']]);
$this->Html->addCrumb(__('Composition de l\'étape'), ['controller' => 'compositions', 'action' => 'index', $etape['Etape']['id']]);

if ($this->Html->value('Composition.id')) {
    $this->Html->addCrumb(__('Modification d\'une composition'));
    echo $this->Bs->tag('h3', __('Modifier la composition de l\'étape du circuit de traitement')) .
    $this->BsForm->create('Cakeflow.Composition', ['url' => [
            'controller' => 'compositions',
            'action' => 'edit', $this->Html->value('Composition.id')]]);
} else {
    $this->Html->addCrumb(__('Ajout d\'une composition'));
    echo $this->Bs->tag('h3', __('Ajouter une composition à l\'étape du circuit de traitement')) .
    $this->BsForm->create('Cakeflow.Composition', ['url' => [
            'controller' => 'compositions',
            'action' => 'add', $etape['Etape']['id']]]);
}

echo $this->Bs->row();
    echo $this->Bs->col('xs6') .
        // sélection du type de composition
        $this->BsForm->select(
            'Composition.type_composition',
            $typeCompositions,
            [
            'empty' => false,
            'autocomplete' => 'off',
            'width' => '100%',
            'class' => 'cakeflow_selectone',
            'data-placeholder' => 'Sélectionner une composition',
            'label' => 'Type de composition']
        )
    . $this->Bs->close();
    echo $this->Bs->col('xs1')
        . $this->Bs->icon('arrow-right')
    . $this->Bs->close();
    echo $this->Bs->col('xs5');
        echo $this->Bs->div(null, $this->BsForm->select('selectUser', $triggers, [
                    'label' => Configure::read('Cakeflow.trigger.title'),
                    'name' => 'Composition.trigger_id',
                    'autocomplete' => 'off',
                    'width' => '100%',
                    'class' => 'cakeflow_selectone',
                    'data-placeholder' => 'Sélectionner un utilisateur',
                    'empty' => false,
                    'value' => $this->Html->value('Composition.trigger_id')]), [
            'id' => 'users', 'style' => '']);
        if (!empty($soustypes)) {
            echo $this->Bs->div(null, $this->BsForm->select('soustype', $soustypes, [
                        'class' => 'cakeflow_selectone',
                        'autocomplete' => 'off',
                        'width' => '100%',
                        'data-placeholder' => 'Sélectionner un sous-type',
                        'label' => Configure::read('IPARAPHEUR_TYPE') ? __('Sous-Types de "%s"', Configure::read('IPARAPHEUR_TYPE')) : __('Sous-Types'),
                        'empty' => false]), ['id' => 'soustypes', 'style' => '']);
        }
echo $this->Bs->close(2);

if (Configure::read('Cakeflow.manage.parapheur')) {
    echo $this->Html->tag('div', "", ['style' => 'float: left; margin-bottom: 20px; clear: both;', 'id' => 'typeValidation']);
    echo $this->BsForm->input('Composition.type_validation', ['type' => 'radio', 'label' => 'Type de validation']);
} else {
    echo $this->BsForm->input('Composition.type_validation', ['type' => 'hidden', 'value' => 'V']);
}
echo($this->BsForm->isFieldError('Composition.type_composition') ? $this->BsForm->error('Composition.type_composition') : '');
echo $this->BsForm->input('Composition.etape_id', ['type' => 'hidden']);
echo $this->BsForm->input('Composition.trigger_id', ['type' => 'hidden']);
echo $this->Html2->btnSaveCancel('', $previous);
?>
<script type="text/javascript">
    require(['domReady'], function (domReady) {
        domReady(function () {
            require(['jquery', '/cakeflow/js/appCakeflow.js'], function ($) {
                $.appCakeflow();
            });
            $("#CompositionSelectUser").on("change", function () {
                $('#CompositionTriggerId').val($('#CompositionSelectUser').val());
            })
<?php
if (Configure::read('Cakeflow.manage.parapheur')) {
    echo '$("#typeValidation").hide();';
    echo '$("#CompositionTypeValidationD").hide();';
    echo '$("label[for=\'CompositionTypeValidationD\']").hide();';
}
?>

            $("#CompositionTypeComposition").on("change", function () {
                var selectedOption = $('#CompositionTypeComposition').val();
                $('#tmp_parapheur').remove();
                $('#CompositionTriggerId').val('-1');
                if (selectedOption == 'USER') {
                    $('#soustypes').hide();
                    $("#CompositionSelectUser").select2({"width":"100%"});
                    //$('#tmp_custom').remove();
                    $("input[name='data[Composition][type_validation]']").val('V');
                    $('#CompositionTriggerId').val($('#CompositionSelectUser').val());
                    $('#users').show();

                } else if (selectedOption == 'PARAPHEUR') {
                    $('#users').hide();
                    $("#CompositionSoustype").select2({"width":"100%"});
                    //$("#CompositionSelectUser").select2("destroy");
                    $('#CompositionSelectUser').append('<option value="-1" id="tmp_parapheur">Parapheur</option>');
                    $("#CompositionSelectUser :selected").prop('selected', false)
                    $('#CompositionSelectUser option[value="-1"]').prop('selected', true);
                    $("input[name='data[Composition][type_validation]']").val('D');
                    $('#CompositionTriggerId').val('-1');
                    $('#soustypes').show();
                } else if (selectedOption == 'PASTELL') {
                    $('#users').hide();
                    $("#CompositionSoustype").select2({"width":"100%"});
                    //$("#CompositionSelectUser").select2("destroy");
                    $('#CompositionSelectUser').append('<option value="-1" id="tmp_parapheur">Pastell</option>');
                    $("#CompositionSelectUser :selected").prop('selected', false)
                    $('#CompositionSelectUser option[value="-1"]').prop('selected', true);
                    $("input[name='data[Composition][type_validation]']").val('D');
                    $('#CompositionTriggerId').val('-1');
                    $('#soustypes').show();
                }
                else {
                    //rajoute un champ ds select user et donc informe du triger_id de la table composition
                    //$('#tmp_custom').remove();
                    //$('#CompositionSelectUser').append('<option value="' + $('#CompositionTypeComposition').val() + '" id="tmp_custom">custom</option>');
                    //selectionne le champ précédement créé
                    //$('#CompositionSelectUser').val($('#CompositionTypeComposition').val());
                    //$("input[name='data[Composition][type_validation]']").val('C');
                    $('#users').hide();
                    $('#soustypes').hide();
<?php if (Configure::read('Cakeflow.manage.parapheur')) {
    echo '$("#typeValidation").hide();';
} ?>
                }
            });
            $("#CompositionTypeComposition").trigger("change");
        });
    });
</script>
