<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

$this->Html->addCrumb(__('Administration'));
$this->Html->addCrumb(__('Utilisateurs'));
$this->Html->addCrumb(__('Circuits'), ['controller' => 'circuits', 'action' => 'index']);
echo $this->Html->css("Cakeflow.design.css");
echo $this->Bs->tag('h3', 'Étapes du circuit : ' . $circuit);
$this->Html->addCrumb(__('Étapes du circuit'));
echo $this->Html2->btnAdd("Ajouter une étape", null, ['controller' => 'etapes', 'action' => 'add', $circuit_id]);
if (!empty($etapes)) {
    echo $this->Bs->table(
            [
        ['title' => __('Ordre')],
        ['title' => __('Nom')],
        ['title' => __('Description')],
        ['title' => __('Type')],
        ['title' => __('Utilisateur(s)')],
        ['title' => __('Délais avant retard')],
        ['title' => __('Actions')]
            ],
        ['striped']
    );
    foreach ($etapes as $rownum => $etape) {
        $row = Set::extract($etape, 'Etape');
        // Liens pour changer la position de l'étape
        $ordre = $etape['Etape']['ordre'];
        $moveUp = $ordre > 1 ? $this->Html->link('&#9650;', ['action' => 'moveUp', $row['id']], ['escape' => false], false) : '&#9650;';
        $moveDown = $ordre < $nbrEtapes ? $this->Html->link('&#9660;', ['action' => 'moveDown', $row['id']], ['escape' => false]) : '&#9660;';
        // Mise en forme de la liste des déclencheurs
        $triggers = [];
        foreach ($etape['Composition'] as $composition) {
            $triggers[] = $composition['libelleTrigger'];
        }
        if (!empty($row['cpt_retard'])) {
            $cells = $this->Bs->icon('clock-o') . ' ' . $row['cpt_retard'] . ' ' . __('jours avant la séance');
        } elseif (!isset($row['cpt_retard'])) {
            $cells = $this->Bs->icon('ban') . ' ' . ' <em>' . __('Pas d\'alerte de retard programmée') . '<em>';
        } else {
            $cells = $this->Bs->icon('clock-o') . ' ' . __('Le jour de la séance');
        }
        //glyphicon-user
        $bouton = $this->Bs->div('btn-group')
                . $this->Bs->btn($this->Bs->icon('user'), ['controller' => 'compositions', 'action' => 'index', $etape['Etape']['id']], [
                    'type' => 'primary',
                    'escapeTitle' => false,
                    'title' => __("Composition de l'étape", true)])
                . $this->Bs->btn($this->Bs->icon('eye'), ['action' => 'view', $etape['Etape']['id']], [
                    'type' => 'default',
                    'escapeTitle' => false,
                    'title' => __('Visualiser')])
                . $this->Bs->btn($this->Bs->icon('pencil-alt'), ['action' => 'edit', $etape['Etape']['id']], [
                    'type' => 'primary',
                    'escapeTitle' => false,
                    'title' => __('Modifier')])
                . $this->Bs->btn($this->Bs->icon('trash'), ['action' => 'delete', $etape['Etape']['id']], [
                    'type' => 'danger',
                    'escapeTitle' => false,
                    'title' => __('Supprimer')], __('Êtes vous sur de vouloir supprimer ') . $etape['Etape']['nom'] . ' ?');
        echo $this->Bs->cell($moveUp . $ordre . $moveDown);
        echo $this->Bs->cell($row['nom']);
        echo $this->Bs->cell($row['description']);
        echo $this->Bs->cell(h($row['libelleType']));
        echo $this->Bs->cell(implode(', ', $triggers));
        echo $this->Bs->cell($cells);
        echo $this->Bs->cell($bouton, 'text-nowrap');
    }
    echo $this->Bs->endTable();

    //paginate
    echo $this->element('paginator', array('paginator' => $this->Paginator));
}
echo $this->Bs->div('actions btn-group col-md-offset-0', null) .
$this->Html2->btnCancel($previous);
$this->Bs->close();
