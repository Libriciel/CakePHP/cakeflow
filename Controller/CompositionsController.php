<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

class CompositionsController extends CakeflowAppController
{
    public $name = 'Compositions';
    public $uses = [
        'Cakeflow.Etape',
        'Cakeflow.Composition',
        //'ConnectorManager.Connector'
        ];
    public $components = [
        'Paginator',
        'Cakeflow.VueDetaillee',
        'Auth' => [
            'mapActions' => [
                'read' => ['manager_index', 'manager_view', 'admin_index', 'admin_view'],
                'create' => ['manager_add', 'admin_add'],
                'update' => ['manager_edit', 'admin_edit'],
                'delete' => ['manager_delete', 'admin_delete'],
            ]
        ],
    ];

    public function admin_index($etape_id = null)
    {
        $this->index($etape_id);

        $this->render('index');
    }

    public function manager_index($etape_id = null)
    {
        $this->index($etape_id, ['id']);

        $this->render('index');
    }

    private function index($etape_id = null, $allow = null)
    { // FIXME: Composition.etape_id
        // lecture de l'étape
        $etape = $this->Composition->Etape->find('first', [
            'fields' => ['id', 'nom'],
            'contain' => ['Circuit.nom', 'Circuit.id'],
            'conditions' => ['Etape.id' => $etape_id],
            'allow' => $allow]);
        if (!empty($etape)) {
            $this->paginate = [
                'recursive' => -1,
                'fields' => ['id', 'type_validation', 'trigger_id', 'soustype', 'type_composition'],
                'conditions' => ['Composition.etape_id' => $etape_id]];
            $compositions = $this->Paginator->paginate('Composition');
            //Si le circuit est vide, rediriger vers la vue d'ajout d'étape
            if (empty($compositions)) {
                $this->History->goBack(1);
                $this->redirect(['action' => 'add', $etape_id]);
            }

            // mise en forme pour la vue
            foreach ($compositions as $i => $data) {
                $compositions[$i]['Composition']['typeValidationLibelle'] = $this->Composition->libelleTypeValidation($compositions[$i]['Composition']['type_validation']);
                $compositions[$i]['Composition']['triggerLibelle'] = $this->Composition->formatLinkedModel(
                        'Trigger',
                    $compositions[$i]['Composition']['trigger_id'],
                    $compositions[$i]['Composition']['type_composition'],
                    $compositions[$i]['Composition']['soustype']
                );
            }

            // Peut-on ajouter une composition à cette étape ?
            $canAdd = $this->Composition->canAdd($etape_id);
            $this->set(compact('etape_id', 'compositions', 'etape', 'canAdd'));
        } else {
            $this->Flash->set('Etape introuvable', ['element' => 'growl']);
            $this->redirect($this->previous);
        }

    }

    public function admin_view($id = null)
    {
        $this->view($id);

        $this->render('view');
    }

    public function manager_view($id = null)
    {
        $this->view($id, ['id']);

        $this->render('view');
    }

    /**
     * Vue détaillée des compositions d'une étape
     */
    private function view($id = null, $allow = null)
    {
        $this->data = $etape = $this->Composition->find('first', [
            'contain' => ['Etape.id', 'Etape.nom', 'Etape.Circuit.nom'],
            'conditions' => ['Composition.id' => $id]]);
        if (empty($this->data)) {
            $this->Flash->set(__('Invalide id pour la', true) . ' ' . __('composition', true) . ' : ' . __('affichage de la vue impossible.', true), ['element' => 'growl','params'=>['type' => 'warning']]);
            $this->redirect(['action' => 'index']);
        } else {
            $this->pageTitle = Configure::read('appName') . ' : ' . __('Composition des Circuits de traitement', true) . ' : ' . __('vue détaillée', true);

            // préparation des informations à afficher dans la vue détaillée
            $maVue = new $this->VueDetaillee(
                    'Vue détaillée de la composition de l\'étape "' . $this->data['Etape']['nom'] . '" du circuit "' . $this->data['Etape']['Circuit']['nom'] . '"',
                __('Retour à la liste des compositions', true),
                ['action' => 'index', $this->data['Etape']['id']]
            );

            if ($this->data['Composition']['trigger_id'] == -1) {
                $maVue->ajouteLigne(
                    __('Sous-Type', true),
                $this->Composition->formatLinkedModel(
                        'Trigger',
                    $this->data['Composition']['trigger_id'],
                    $this->data['Composition']['type_composition'],
                    $this->data['Composition']['soustype']
                )
                );
            }
            $maVue->ajouteSection(__('Création / Modification', true));
            $maVue->ajouteLigne(__('Date de création', true), $this->data['Composition']['created']);
            $maVue->ajouteElement(__('Par', true), $this->Composition->formatUser($this->data['Composition']['created_user_id']));
            $maVue->ajouteLigne(__('Date de dernière modification', true), $this->data['Composition']['modified']);
            $maVue->ajouteElement(__('Par', true), $this->Composition->formatUser($this->data['Composition']['modified_user_id']));


            $maVue->ajouteSection(__('Informations principales', true));
            $maVue->ajouteLigne(__('Identifiant interne (id)', true), $this->data['Composition']['id']);
            if ($this->data['Composition']['trigger_id'] == -1) {
                $maVue->ajouteLigne("Déclencheur", "Parapheur électronique");
            } else {
                $maVue->ajouteLigne(Configure::read('Cakeflow.trigger.title'), $this->Composition->formatLinkedModel('Trigger', $this->data['Composition']['trigger_id']));
            }
            $maVue->ajouteLigne(__('Type', true), $this->Composition->libelleTypeValidation($this->data['Composition']['type_validation']));

            $this->set('contenuVue', $maVue->getContenuVue());
        }
        $this->set('etape', $etape);
    }

    public function admin_add($id = null)
    {
        $this->add($id);
        $this->render('add_edit');
    }

    public function manager_add($id = null)
    {
        $this->add($id);
        $this->render('add_edit');
    }

    public function admin_edit($id = null)
    {
        $this->edit($id);
        $this->render('add_edit');
    }

    public function manager_edit($id = null)
    {
        $this->edit($id);
        $this->render('add_edit');
        ;
    }

    /**
     *
     */
    private function add($etape_id = null)
    {
        $etape = $this->Etape->find('first', [
            'fields' => ['id', 'nom'],
            'contain' => ['Circuit' => ['fields' => ('nom')]],
            'conditions' => ['Etape.id' => $etape_id],
            'recursive' => -1
        ]);

        if (!$etape_id) {
            throw new NotFoundException(__('Invalid Etape'));
        }

        if ($this->request->is('post')) {
            $this->Composition->create();

            if ($this->request->data['Composition']['type_composition'] == 'USER') {
                $this->request->data['Composition']['soustype'] = null;
            } elseif ($this->request->data['Composition']['type_composition'] == 'PARAPHEUR') {
            } elseif ($this->request->data['Composition']['type_composition'] == 'PASTELL') {
            } else {
                $this->request->data['Composition']['soustype'] = $this->request->data['Composition']['type_composition'];
                $this->request->data['Composition']['type_composition'] = 'CONNECTOR';
            }

            if ($this->Composition->save($this->request->data)) {
                $this->Flash->set(__('La composition a été enregistrée.'), ['element' => 'growl','params' => ['type' => 'success']]);

                return $this->redirect(['action' => 'index', $this->data['Composition']['etape_id']]);
            } else {
                //debug($this->Composition->validationErrors);exit;
                $this->Flash->set('Impossible d\'ajouter une composition.', ['element' => 'growl','params'=>['type' => 'danger']]);
                if ($this->request->data['Composition']['type_composition'] == 'CONNECTOR') {
                    $this->request->data['Composition']['type_composition'] = $this->request->data['Composition']['soustype'];
                }
            }
        }
        $this->request->data['Composition']['etape_id'] = $etape_id;

        $this->set('etape', $etape);
        $this->set('circuit_id', $etape['Circuit']['id']);
        $this->listTriggers();
        $this->set('typeValidations', $this->Composition->listeTypeValidation());
    }

    /**
     *
     */
    private function edit($id = null)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid Composition'));
        }

        $this->Composition->recursive = -1;
        $composition = $this->Composition->findById($id);
        $etape = $this->Etape->find('first', [
            'fields' => ['id', 'nom'],
            'contain' => ['Circuit' => ['fields' => ('nom')]],
            'conditions' => ['Etape.id' => $composition['Composition']['etape_id']],
            'recursive' => -1
        ]);

        if (!$etape) {
            throw new NotFoundException(__('Invalid Etape'));
        }

        if ($this->request->is(['post', 'put'])) {
            $this->request->data['Composition']['id'] = $id;

            if ($this->request->data['Composition']['type_composition'] == 'USER') {
                $this->request->data['Composition']['soustype'] = null;
            } elseif ($this->request->data['Composition']['type_composition'] == 'PARAPHEUR') {
            } elseif ($this->request->data['Composition']['type_composition'] == 'PASTELL') {
            } else {
                $this->request->data['Composition']['soustype'] = $this->request->data['Composition']['type_composition'];
                $this->request->data['Composition']['type_composition'] = 'CONNECTOR';
                $this->request->data['Composition']['type_validation'] = 'D';
                $this->request->data['Composition']['trigger_id'] = '-1';
            }

            if ($this->Composition->save($this->request->data)) {
                $this->Flash->set(__('La composition a été enregistrée.'), ['element' => 'growl','params' => ['type' => 'success']]);

                return $this->redirect(['action' => 'index', $this->data['Composition']['etape_id']]);
            } else {
                //debug($this->Composition->validationErrors);
                $this->Flash->set(__('Impossible de mettre à jour la composition.'), ['element' => 'growl','params' => ['type' => 'danger']]);
            }
        }

        if (!$this->request->data) {
            $this->request->data = $composition;
        }
        if ($this->request->data['Composition']['type_composition'] == 'CONNECTOR') {
            $this->request->data['Composition']['type_composition'] = $this->request->data['Composition']['soustype'];
        }

        $this->set('etape', $etape);
        $this->set('circuit_id', $etape['Circuit']['id']);
        $this->listTriggers();
        $this->set('typeValidations', $this->Composition->listeTypeValidation());
    }

    public function admin_delete($id = null)
    {
        $this->delete($id);
    }

    public function manager_delete($id = null)
    {
        $this->delete($id, ['id']);
    }

    /**
     *
     */
    private function delete($id = null, $allow = null)
    {
        if ($this->Composition->delete($id)) {
            $this->Flash->set(__('Suppression effectuée', true), ['element' => 'growl']);
        } else {
            $this->Flash->set('Erreur lors de la suppression');
        }
        return $this->redirect($this->previous);
    }

    /**
     * Retourne la liste des types de composition
     * @version 5.3.0
     * $id int composition_id
     * @return array code, libelle de la liste des types de composition
     */
    private function listTriggers()
    {
        $typeCompositions = ['USER' => __('Utilisateur de %s', Configure::read('Cakeflow.app'))];
        // lecture de la liste des déclencheurs
        $triggers = $this->Composition->listLinkedModel('Trigger');
        $this->set('triggers', $triggers);

        if (Configure::read('USE_PARAPHEUR')) {
            try {
                $soustypes = $this->Composition->listeVisaSousTypesParapheur();
                if(!empty($soustypes)){
                    if(Configure::read('PARAPHEUR')==='IPARAPHEUR') {
                        $typeCompositions['PARAPHEUR'] = __('Connecteur : Parapheur électronique', true);
                    }
                    if(Configure::read('PARAPHEUR')==='PASTELL') {
                        $typeCompositions['PASTELL'] = __('Connecteur Pastell : Parapheur électronique', true);
                    }

                    $this->set('soustypes', $soustypes);
                }
            } catch (Exception $e) {
                unset($typeCompositions['PARAPHEUR']);
                CakeLog::error($e->getMessage());
            }
        }

//        $connectors = $this->Connector->find('all', array(
//            'fields' => array('id', 'name', 'config_json'),
//            'recursive' => -1,
//        ));
//
//        foreach ($connectors as $connector) {
//            $json = json_decode($connector['Connector']['config_json']);
//            if (isset($json->config->model) && $json->config->model == $this->plugin . '.Composition') {
//                $typeCompositions[$connector['Connector']['id']] = __('Connecteur : %s', $connector['Connector']['name']);
//            }
//        }

        $this->set('typeCompositions', $typeCompositions);
    }
}
