<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * Plugin Workflow for CakePHP: Préparation du contenu des vues détaillées (view) dans les controleurs.
 *
 * @package app.Controller.Component
 * @version v4.1.0
 */

class VueDetailleeComponent extends Component
{
    private $contenuVue = [];

    public function __construct($titreVue = 'Vue d&eacute;taill&eacute;e', $lienRetourTitle = 'Retour', $lienRetourUrl = ['action' => 'index'])
    {
        $this->contenuVue['titreVue'] = $titreVue;
        $this->contenuVue['lienRetour'] = [
            'title' => $lienRetourTitle,
            'url' => $lienRetourUrl];
        $this->contenuVue['sections'] = [];
    }

    /*
     * Ajoute une section générale
     */
    public function ajouteSection($nom = '')
    {
        $this->contenuVue['sections'][] = [
            'titreSection' => $nom,
            'lignes' => []
        ];
    }

    /*
     * ajoute une nouvelle ligne à la dernière section
     */
    public function ajouteLigne($libelle, $valeur = '', $class='')
    {
        $iSection = count($this->contenuVue['sections'])-1;
        $this->contenuVue['sections'][$iSection]['lignes'][][] = [
            'libelle' => $libelle,
            'valeur' => $valeur,
            'class' => $class
        ];
    }

    /*
     * ajoute un nouvel élément à la dernière ligne de la dernière section
     */
    public function ajouteElement($libelle, $valeur = '')
    {
        $iSection = count($this->contenuVue['sections'])-1;
        $iLigne = count($this->contenuVue['sections'][$iSection]['lignes'])-1;
        $this->contenuVue['sections'][$iSection]['lignes'][$iLigne][] = [
            'libelle' => $libelle,
            'valeur' => $valeur
        ];
    }

    /*
     * retourne le contenue de la vue
     */
    public function getContenuVue()
    {
        return $this->contenuVue;
    }
}
