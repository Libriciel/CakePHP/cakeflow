<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

/**
 * [CircuitsController description]
 * @version 5.3.0
 * @since   4.1.0
 */
class CircuitsController extends CakeflowAppController
{
    /**
     * Components
     * @version 5.3.0
     * @var array
     */
    public $components = [
        'AuthManager.AclManager',
        'Cakeflow.VueDetaillee',
        'Paginator',
        'Auth' => [
            'mapActions' => [
                'read' => [
                  'view',
                  'manager_index',
                  'manager_view',
                  'admin_index',
                  'admin_view'
                ],
                'create' => ['manager_add', 'admin_add', 'manager_duplicate', 'admin_duplicate'],
                'update' => ['manager_edit', 'admin_edit'],
                'delete' => ['manager_delete', 'admin_delete'],
                'allow' => ['visuCircuit'],
            ]
        ],
        'Filtre',
    ];
    /**
     * Uses
     * @version 5.3.0
     * @var array
     */
    public $uses = ['Cakeflow.Circuit',
        'Cakeflow.Etape',
        'Cakeflow.Visa',
        'Cakeflow.Composition',
        'Cakeflow.Traitement',
        'User',
        'Aco', 'Aro'];

    /**
     * [admin_index description]
     * @version 5.3
     * @return [type] [description]
     */
    public function admin_index()
    {
        $this->index();

        $this->render('index');
    }
    /**
     * [manager_index description]
     * @version 5.3
     * @return [type] [description]
     */
    public function manager_index()
    {
        $this->index(['id']);

        $this->render('index');
    }

    /**
     * Liste des circuits de traitement
     * @version 5.3
     */
    private function index($allow = null)
    {
        $this->pageTitle = Configure::read('appName') . ' : ' . __('Circuits de traitement', true) . ' : ' . __('liste', true);
        $this->Filtre->initialisation($this->name . ':' . $this->action, $this->data);
        if (!$this->Filtre->critereExists()) {
            $circuits = $this->Circuit->find('list', [
                'fields' => ['id', 'nom'],
                'order' => 'nom',
                'recursive' => -1,
                'allow' => $allow
            ]);
            $this->Filtre->addCritere('Circuit', [
                'field' => 'Circuit.id',
                'classeDiv' => 'demi',
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Nom du circuit'),
                    'title' => __('Recherche par nom de circuit'),
                    'data-placeholder' => __('Sélectionner un circuit'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'options' => $circuits
                  ]
            ]);

            $this->Filtre->addCritere('Actif', [
                'field' => 'Circuit.actif',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Actif'),
                    'options' => [0 => __('Non'), 1 => __('Oui')],
                    'data-placeholder' => __('Sélectionner un état'),
                    'data-allow-clear' => true,
                    'title' => __('Filtre sur les états des circuits')
                  ]
            ]);

            //Définition d'un champ virtuel pour affichage complet des informations
            $this->User->virtualFields['name'] = 'User.prenom || \' \' || User.nom || \' (\' || User.username || \')\'';

            $users = $this->User->find('list', [
                'fields' => ['id', 'name'],
                'order' => 'username',
                'recursive' => -1,
                'allow' => $allow
            ]);

            $this->Filtre->addCritere('Utilisateur', [
                'field' => 'User.id',
                'classeDiv' => 'demi',
                'retourLigne' => true,
                'inputOptions' => [
                    'type' => 'select',
                    'label' => __('Nom d\'un utilisateur'),
                    'title' => __('Recherche par nom, prénom et login'),
                    'data-placeholder' => __('Sélectionner un utilisateur'),
                    'data-allow-clear' => true,
                    'multiple' => true,
                    'options' => $users
                  ]
            ]);
        }

        $conditions = $this->Filtre->conditions();

        if (isset($conditions['AND']['User.id'])) {
            $listeCircuitsParUtilisateur = explode(',', $this->Circuit->listeCircuitsParUtilisateur($conditions['AND']['User.id']));
            $conditions['AND']['Circuit.id'] =  isset($conditions['AND']['Circuit.id']) ? array_intersect($conditions['AND']['Circuit.id'], $listeCircuitsParUtilisateur) : $listeCircuitsParUtilisateur;
            unset($conditions['AND']['User.id']);
        }


        $this->paginate = [
            'fields' => ['id', 'nom', 'description', 'actif', 'defaut'],
            'conditions'=> $conditions,
            'order' => ['Circuit.nom' => 'asc'],
            'recursive' => -1,
            'allow' => $allow,
            'page' => 1
        ];
        $this->request->data = $this->Paginator->paginate($this->Circuit);

        // lecture des étapes dans l'odre 'ordre'
        foreach ($this->request->data as &$circuit) {
            $circuit['Etape'] = $this->Circuit->Etape->find('all', [
                'fields' => ['nom', 'type'],
                'conditions' => ['circuit_id' => $circuit['Circuit']['id']],
                'order' => ['ordre'],
                'recursive' => -1
              ]);
            foreach ($circuit['Etape'] as &$etape) {
                $etape['Etape']['label'] = Etape::getTypeLabelByValue($etape['Etape']['type']);
            }
            $circuit['ListeActions']['view'] = true;
            $circuit['ListeActions']['visuCircuit'] = true;
            $circuit['ListeActions']['edit'] = true;
            $circuit['ListeActions']['delete'] = $this->Circuit->isDeletable($circuit['Circuit']['id']);
            $circuit['Circuit']['actifLibelle'] = $this->Circuit->boolToString($circuit['Circuit']['actif']);
            $circuit['Circuit']['defautLibelle'] = $this->Circuit->boolToString($circuit['Circuit']['defaut']);
        }
    }
    /**
     * [admin_view description]
     * @version 5.3
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function admin_view($id = null)
    {
        $this->view($id);

        $this->render('view');
    }

    /**
     * [manager_view description]
     * @version 5.3
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function manager_view($id = null)
    {
        $this->view($id, ['id']);

        $this->render('view');
    }

    /**
     * Vue détaillée des circuits de traitement
     * @version 5.3
     */
    private function view($id = null, $allow = null)
    {
        $this->request->data = $this->Circuit->find('first', [
            'conditions' => ['Circuit.id' => $id],
            'allow' => $allow,
            'recursive' => -1]);
        if (empty($this->data)) {
            $this->Flash->set(__('Invalide id pour le', true) . ' ' . __('circuit de traitement', true) . ' : ' . __('affichage de la vue impossible.', true), ['element' => 'growl', 'params' => ['type' => 'warning']]);
            $this->redirect(['action' => 'index']);
        } else {
            $this->pageTitle = Configure::read('appName') . ' : ' . __('Circuits de traitement', true) . ' : ' . __('vue détaillée', true);

            // préparation des informations à afficher dans la vue détaillée
            $maVue = new $this->VueDetaillee(
                    __('Vue détaillée du circuit', true) . ' : ' . $this->data['Circuit']['nom'],
                __('Retour à la liste des circuits de traitement', true)
            );

            $maVue->ajouteSection(__('Création / Modification'));
            $maVue->ajouteLigne(__('Date de création'), $this->data['Circuit']['created']);
            $maVue->ajouteElement(__('Par'), $this->Circuit->formatUser($this->data['Circuit']['created_user_id']));
            $maVue->ajouteLigne(__('Date de dernière modification'), $this->data['Circuit']['modified']);
            $maVue->ajouteElement(__('Par'), $this->Circuit->formatUser($this->data['Circuit']['modified_user_id']));

            $maVue->ajouteSection(__('Informations principales'));
            $maVue->ajouteLigne(__('Identifiant interne (id)'), $this->data['Circuit']['id']);
            $maVue->ajouteLigne(__('Nom'), $this->data['Circuit']['nom']);
            $maVue->ajouteLigne(__('Description'), $this->data['Circuit']['description']);
            if (Configure::read('Cakeflow.manage.default')) {
                $maVue->ajouteLigne(__('Défaut'), $this->Circuit->boolToString($this->data['Circuit']['defaut']));
            }
            $maVue->ajouteLigne(__('Actif'), $this->Circuit->boolToString($this->data['Circuit']['actif']));

            // Affichage des étapes liées
            $etapes = $this->Circuit->Etape->find('all', [
                'conditions' => ['Etape.circuit_id' => $id],
                'contain' => ['Composition.type_validation', 'Composition.trigger_id'],
                'fields' => ['Etape.nom', 'Etape.type'],
                'order' => ['Etape.ordre'],
            ]);
            if (!empty($etapes)) {
                $this->visuCircuit($id);
            }

            $this->set('contenuVue', $maVue->getContenuVue());
        }
    }

    /**
     * [admin_add description]
     * @version 5.3
     */
    public function admin_add()
    {
        $this->add_edit();

        $this->render('add_edit');
    }
    /**
     * [manager_add description]
     * @version 5.3
     */
    public function manager_add()
    {
        $this->add_edit();

        $this->render('add_edit');
    }
    /**
     * [manager_edit description]
     * @version 5.3
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function manager_edit($id = null)
    {
        $this->add_edit($id);

        $this->render('add_edit');
    }

    /**
     * [admin_edit description]
     * @version 5.3
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function admin_edit($id = null)
    {
        $this->add_edit($id);

        $this->render('add_edit');
    }
    /**
     * [add_edit description]
     * @version 5.3
     * @param [type] $id [description]
     */
    private function add_edit($id = null)
    {
        if (!empty($this->data)) {
            $this->setCreatedModifiedUser($this->request->data);
            $this->Circuit->create($this->data);
            if ($this->Circuit->validates($this->data)) {
                if ($this->Circuit->save()) {
                    if (empty($id)) {
                        $aAcoAro = [];

                        $this->Aco->recursive = -1;
                        $aAcoCircuit = $this->Aco->findByForeignKeyAndModel($this->Circuit->id, 'Circuit');

                        $this->Aro->recursive = -1;
                        $aAroUser = $this->Aro->findByForeignKeyAndModel($this->Auth->user('id'), 'User');

                        $this->Aro->Permission->create();
                        $aAcoAro['Permission']['aro_id'] = $aAroUser['Aro']['id'];
                        $aAcoAro['Permission']['aco_id'] = $aroUser = $aAcoCircuit['Aco']['id'];
                        foreach ($this->AclManager->getKeys() as $key => $perm) {
                            $aAcoAro['Permission'][$perm] = 1;
                        }
                        $this->Aro->Permission->save($aAcoAro);
                        $this->Flash->set(__('Le circuit %s a été ajouté', $this->data['Circuit']['nom']), [
                            'element' => 'growl',
                            'params' => ['type' => 'success']
                        ]);
                    } else {
                        $this->Flash->set(__('Le circuit %s a été modifié', $this->data['Circuit']['nom']), [
                            'element' => 'growl',
                            'params' => ['type' => 'success']
                        ]);
                    }
                    return $this->redirect($this->previous);
                } else {
                    $this->Flash->set(__('Erreur lors de l\'enregistrement'), [
                        'element' => 'growl',
                        'params' => ['type' => 'danger']]);
                }
            } else {
                $this->Flash->set(__('Veuillez corriger les erreurs du formulaire'), [
                    'element' => 'growl',
                    'params' => ['type' => 'danger']]);
            }
        }

        if ($this->action == 'admin_add' or $this->action == 'manager_add') {
            $this->request->data['Circuit']['actif'] = true;
        }
        if ($this->action == 'admin_edit' or $this->action == 'manager_edit') {
            $this->request->data = $this->Circuit->read(null, $id);
        }

        $this->render('add_edit');
    }
    /**
     * [admin_delete description]
     * @version 5.3
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function admin_delete($id = null)
    {
        $this->delete($id);
    }

    /**
     * [manager_delete description]
     * @version 5.3
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function manager_delete($id = null)
    {
        $this->delete($id, ['id']);
    }
    /**
     * [delete description]
     * @version 5.3
     * @param  [type] $id    [description]
     * @param  [type] $allow [description]
     * @return [type]        [description]
     */
    private function delete($id = null, $allow = null)
    {
        if ($this->Circuit->delete($id)) {
            $this->Flash->set(__('Suppression effectuée'), ['element' => 'growl', 'params' => ['type' => 'warning']]);
        } else {
            $this->Flash->set(__('Erreur lors de la suppression'), ['element' => 'growl', 'params' => ['type' => 'danger']]);
        }
        return $this->redirect($this->previous);
    }

    /**
     * Suppression d'un circuit de traitement
     * @version 5.3
     */
    private function admin($id = null, $allow)
    {
        $eleASupprimer = $this->Circuit->find('first', [
            'conditions' => ['Circuit.id' => $id],
            'allow' => $allow,
            'recursive' => 0]);
        if (empty($eleASupprimer)) {
            $this->Flash->set(__('Invalide id pour la suppression du circuit de traitement'), ['element' => 'growl', 'params' => ['type' => 'danger']]);
        } elseif (!$this->Circuit->isDeletable($id)) {
            $this->Flash->set(__('Le circuit de traitement %s ne peut pas être supprimé', $eleASupprimer['Circuit']['nom']), 'growl');
        } elseif (!$this->Circuit->delete($id, true)) {
            $this->Flash->set(__('Une erreur est survenue pendant la suppression'), ['element' => 'growl', 'params' => ['type' => 'danger']]);
        } else {
            $this->Flash->set(__('Le circuit de traitement %s a été supprimé', $eleASupprimer['Circuit']['nom']), 'growl');
        }
        $this->redirect(['action' => 'index']);
    }

    /**
     * Affiche graphique un circuit de validation
     * Paramètre : id
     * @version 5.3
     */
    public function visuCircuit($circuit_id)
    {
        // lecture des étapes du circuit
        $etapes = $this->Circuit->Etape->find('all', [
            'fields' => ['nom', 'ordre', 'type', 'soustype'],
            'contain' => [
              'Composition'=> ['type_validation', 'trigger_id', 'type_composition', 'soustype']
            ],
            'conditions' => ['circuit_id' => $circuit_id],
            'recursive'=> -1,
            'order' => ['ordre'=>'ASC']]);
        foreach ($etapes as &$etape) {
            $etape['Etape']['libelleType'] = Etape::getTypeLabelByValue($etape['Etape']['type']);
            foreach ($etape['Composition'] as &$composition) {
                $composition['libelleTypeValidation'] = $this->Circuit->Etape->Composition->libelleTypeValidation($composition['type_validation']);
                if ($composition['type_validation'] == 'D' && $composition['soustype'] !== null) {
                    try {
                        $tooltip = Configure::read('IPARAPHEUR_TYPE') . " / " . $this->Circuit->Etape->Composition->libelleSousType($composition['soustype']);
                        $composition['libelleTrigger'] = '<a class="infobulle" data-placement="right" data-toggle="tooltip" title="' . $tooltip . '">' . $this->Circuit->formatLinkedModel('Trigger', $composition['trigger_id']) . "</a>";
                    } catch (Exception $e) {
                        $tooltip = $e->getMessage();
                        $composition['libelleTrigger'] = '<a class="infobulle" data-placement="right" data-toggle="tooltip" title="' . $tooltip . '"><i class="fa fa-warning"></i> Erreur</a>';
                        $composition['libelleTrigger'] .= '<input type="hidden" class="parapheur_error" value="true" />';
                        $this->Flash->set("Problème de connexion au parapheur", 'growl');
                    }
                } else {
                    $composition['libelleTrigger'] = $this->Circuit->formatLinkedModel(
                            'Trigger',
                        $composition['trigger_id'],
                        $composition['type_composition'],
                        $composition['soustype']
                    );
                }
            }
        }
        $this->set('etapes', $etapes);
    }

    /**
     * Duplicate workflow for admin
     * @version 5.3
     */
    public function admin_duplicate($id)
    {
        $this->duplicate($id);
    }

    /**
     * Duplicate workflow for manager
     * @version 5.3
     */
    public function manager_duplicate($id)
    {
        $this->duplicate($id);
    }

    /**
     * Duplicate workflow
     * @version 5.3
     */
    private function duplicate($id)
    {
        if ($this->Circuit->duplicate($id)) {
            $this->Flash->set(__('Le circuit a été dupliqué'), ['element' => 'growl', 'params' => ['type' => 'info']]);
        } else {
            $this->Flash->set(__('Erreur lors de la duplication du circuit'), ['element' => 'growl', 'params' => ['type' => 'danger']]);
        }

        $this->redirect(['action' => 'index']);
    }
}
