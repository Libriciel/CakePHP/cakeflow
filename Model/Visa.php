<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Etape', 'Cakeflow.Model');

/**
 * Plugin Workflow for CakePHP
 *
 * @package app.Model
 * @version 5.3.0
 * @since 4.1.0
 */
class Visa extends CakeflowAppModel
{
    public $tablePrefix = 'wkf_';
    public $belongsTo = [
        CAKEFLOW_TRIGGER_MODEL => [
            'className' => CAKEFLOW_TRIGGER_MODEL,
            'foreignKey' => 'trigger_id'
        ],
        'Cakeflow.Traitement',
        'Cakeflow.Etape',
    ];

//    public $hasOne = array('Cakeflow.Signature');

    public function enregistre($traitement_id, $trigger_id, $etape_id, $commentaire = null, $date, $numero_traitement)
    {
        $composition = $this->Composition->find('first', [
            'conditions' => [
                'Composition.etape_id' => $etape_id,
                'Composition.trigger_id' => $trigger_id]]);
        if (!empty($composition)) {
            $visa = $this->create();
            $visa['Visa']['traitement_id'] = $traitement_id;
            $visa['Visa']['trigger_id'] = $trigger_id;
            $visa['Visa']['etape_id'] = $etape_id;
            $visa['Visa']['composition_id'] = $composition['Composition']['id'];
            $visa['Visa']['commentaire'] = $commentaire;
            $visa['Visa']['date'] = $date;
            $visa['Visa']['numero_traitement'] = $numero_traitement;
            return ($this->save($visa['Visa']));
        } else {
            return false;
        }
    }

    /**
     * Retourne la liste des actions lors de la validation : Accepter(A), Refuser(R), Etape antèrieures (E)
     * @return array code, libelle des actions des traitements
     */
    public function listeAction()
    {
        return [
            'RI' => __d('cakeflow', 'Indéterminé'),
            'OK' => __d('cakeflow', 'Accepter'),
            'KO' => __d('cakeflow', 'Refuser'),
            'IL' => __d('cakeflow', 'Insérer un lacet'),
            'IP' => __d('cakeflow', 'Ajouter des étapes'),
            'JP' => __d('cakeflow', 'Retourner à une étape précédente'),
            'JS' => __d('cakeflow', 'Aller à une étape suivante'),
            'ST' => __d('cakeflow', 'Terminer le traitement'),
            'IN' => __d('cakeflow', 'Inserer dans le circuit de traitement'),
            'VF' => __d('cakeflow', 'Validation finale')
        ];
    }

    /**
     * Retourne la liste des actions à afficher dans l'historique
     * @return array code, libelle des actions des traitements
     */
    public function listeActionEffectuee()
    {
        return [
            'RI' => __d('cakeflow', 'Indéterminé'),
            'OK' => __d('cakeflow', 'Validé'),
            'KO' => __d('cakeflow', 'Refusé'),
            'IL' => __d('cakeflow', 'Lacet inséré'),
            'IP' => __d('cakeflow', 'Etape ajoutée'),
            'JP' => __d('cakeflow', 'Retour à une étape précédente'),
            'JS' => __d('cakeflow', 'Saut d\'étape'),
            'ST' => __d('cakeflow', 'Traitement terminé'),
            'IN' => __d('cakeflow', 'Inséré dans le circuit de traitement'),
            'VF' => __d('cakeflow', 'Validation finale')
        ];
    }

    /**
     * Retourne la liste des actions lors de la validation : Accepter(A), Refuser(R)
     * @return array code, libelle des actions des traitements
     */
    public function listeActionAR()
    {
        return [
            'OK' => __d('cakeflow', 'Accepter'),
            'KO' => __d('cakeflow', 'Refuser')];
    }

    /**
     * Retourne le libellé du type de validation
     * @param string $code_type lettre S ou V
     */
    public function libelleAction($code_type)
    {
        $actions = $this->listeAction();
        return $actions[$code_type];
    }

    /**
     * Retourne le libellé du type de validation
     * @param string $code_type lettre S ou V
     */
    public function libelleActionHistorique($code_type)
    {
        $actions = $this->listeActionEffectuee();
        return $actions[$code_type];
    }

    /**
     * Enregistre toutes les compositions d'un circuit dans la table visas
     * @param int $circuit_id : Identificant du circuit
     * @return bool true
     */
    public function injectArchive($circuit_id)
    {
        $etapes = $this->Etape->find('all', ['conditions' => ['Etape.circuit_id' => $circuit_id]]);
        foreach ($etapes as $etape) {
            $compositions = $this->Composition->find('all', ['conditions' => ['Composition.etape_id' => $etape['Etape']['id']]]);
            foreach ($compositions as $composition) {
                $visa = $this->create();
                $visa['Visa']['traitement_id'] = 999;
                $visa['Visa']['trigger_id'] = $composition[CAKEFLOW_TRIGGER_MODEL]['id'];
                $visa['Visa']['etape_id'] = $composition['Etape']['id'];
                $visa['Visa']['composition_id'] = $composition['Composition']['id'];
                $visa['Visa']['commentaire'] = 'in process...';
                $visa['Visa']['date'] = 0;
                $this->save($visa['Visa']);
            }
        }
        return true;
    }

    /**
     * @param $target_id
     * @param null $user_id si null, 0 remplacé par l'id du rédacteur (premier visa)
     * @return bool
     */
    public function replaceDynamicTrigger($target_id, $users)
    {
        $traitement = $this->Traitement->find('first', [
            'conditions' => ['Traitement.target_id' => $target_id],
            'fields' => ['Traitement.id'],
            'recursive' => -1]);

        $visas = $this->find('all', [
            'conditions' => ['Visa.traitement_id' => $traitement['Traitement']['id']],
            'recursive' => -1,
            'order' => ['Visa.numero_traitement' => 'ASC']]);

        // vérifie si l'on a un seul rédacteur pour l'étape mon rédacteur
        if (count($users)===1) {
            foreach ($visas as $visa) {
                if ($visa['Visa']['trigger_id'] == 0) {
                    $this->id = $visa['Visa']['id'];
                    $this->saveField('trigger_id', $users[0]);
                }
            }
            return true;
        }
        //
        foreach ($visas as $visa) {
            if ($visa['Visa']['trigger_id'] == 0) {
                foreach ($users as $key => $redactorId) {
                    if ($key == 0) {
                        continue;
                    }
                    $this->create();
                    $newVisa = $visa;
                    $newVisa['Visa']['trigger_id'] = $redactorId;
                    $newVisa['Visa']['type_validation'] = 'V';
                    $newVisa['Visa']['action'] = 'RI';
                    $newVisa['Visa']['date'] = null;
                    $newVisa['Visa']['etape_type'] = Etape::getTypeValue('concurrent');
                    unset($newVisa['Visa']['id']);
                    $this->save($newVisa);
                    $this->clear();
                }
                $this->Etape->id = $visa['Visa']['etape_id'];
                $this->Etape->saveField('etape_type', Etape::getTypeValue('concurrent'));

                $this->id = $visa['Visa']['id'];
                $this->saveField('etape_type', Etape::getTypeValue('concurrent'));
                $this->saveField('type_validation', 'V');
                $this->saveField('trigger_id', $users[0]);
            }
        }

        return true;
    }

    public function getAutresVisasEtape($visa)
    {
        return $this->find('all', [
                    'recursive' => -1,
                    'fields' => ['id', 'action'],
                    'conditions' => [
                        'traitement_id' => $visa['Visa']['traitement_id'],
                        'numero_traitement' => $visa['Visa']['numero_traitement'],
                        'id !=' => $visa['Visa']['id']
                    ]
        ]);
    }

    public function visasParallelesValides($visa)
    {
        if ($visa['Visa']['etape_type'] == Etape::getTypeValue('collaboratif')) {
            $visasParalleles = $this->getAutresVisasEtape($visa);
            foreach ($visasParalleles as $v) {
                if ($v['Visa']['action'] == 'RI') {
                    return false;
                }
            }
        }
        return true;
    }

    public function isLastEtape($visa)
    {
        return !$this->hasAny([
                    'traitement_id' => $visa['Visa']['traitement_id'],
                    'numero_traitement >' => $visa['Visa']['numero_traitement'],
        ]);
    }

    /**
     * Liste les utilisateurs du circuit a l'étape courante
     * @param integer $targetId identifiant de projet
     * @return array
     */
    public function whoIs($id, $position = 'current')
    {
        $visa = $this->findById($id, ['numero_traitement', 'traitement_id', 'action']);
        $numero_traitement = $visa['Visa']['numero_traitement'];
        $traitement_id = $visa['Visa']['traitement_id'];

        switch ($position) {
            case 'previous':
                $numero_traitement -= 1;
                break;
            case 'next':
                //Si il y a un saut d'étape
                if ($visa['Visa']['action'] == 'JS') {
                    $visa = $this->find('first', [
                        'fields' => ['numero_traitement'],
                        'conditions' => [
                            'Visa.action <>' => 'RI',
                            'Visa.traitement_id' => $traitement_id,
                            'Visa.numero_traitement >' => $numero_traitement],
                        'order' => 'Visa.numero_traitement ASC',
                        'recursive' => -1]);
                    $numero_traitement = $visa['Visa']['numero_traitement'];
                } else {
                    $numero_traitement += 1;
                }
                break;
            case 'current':
                break;
            default:
                return false;
        }

        $visas = $this->find('all', [
            'conditions' => [
                'Visa.traitement_id' => $traitement_id,
                'Visa.numero_traitement' => $numero_traitement],
            'recursive' => -1]);

        $members = [];
        foreach ($visas as $visa) {
            $members[] = $visa['Visa']['trigger_id'];
        }

        return $members;
    }
}
