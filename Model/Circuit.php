<?php

/**
 * CakeFlow : Plugin Workflow for CakePHP
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/plugins-cakephp/ plugins-cakephp Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Etape', 'Cakeflow.Model');

/**
 * Plugin Workflow for CakePHP
 *
 * @package app.Model
 * @version v4.1.0
 */
class Circuit extends CakeflowAppModel
{
    public $tablePrefix = 'wkf_';
    public $displayField = 'nom';

    public $belongsTo = [
        'CreatedUser' => [
            'className' => CAKEFLOW_USER_MODEL,
            'foreignKey' => 'created_user_id',
            'conditions' => '',
            'order' => '',
            'dependent' => false
        ],
        'ModifiedUser' => [
            'className' => CAKEFLOW_USER_MODEL,
            'foreignKey' => 'modified_user_id',
            'conditions' => '',
            'order' => '',
            'dependent' => false
        ]
    ];
    public $hasMany = [
        'Etape' => [
            'className' => 'Cakeflow.Etape',
            'foreignKey' => 'circuit_id',
            'conditions' => '',
            'order' => 'Etape.ordre ASC',
            'dependent' => true
        ],
        'Traitement' => [
            'className' => 'Cakeflow.Traitement',
            'foreignKey' => 'circuit_id',
            'conditions' => '',
            'order' => '',
            'dependent' => false
        ]
    ];

    public $validate = [
        'nom' => [
            [
                'rule' => ['maxLength', '250'],
                'message' => 'Maximum 250 caractères'
            ],
            [
                'rule' => 'isUnique',
                'message' => 'Valeur déjà utilisée'
            ],
            [
                'rule' => 'notBlank',
                'required' => true,
                'message' => 'Champ obligatoire'
            ]
        ]
    ];
    public $actsAs = ['AuthManager.AclManager' => ['type' => 'controlled']];

    /**
    * les regles de controle et les messages d'erreurs de la variable de classe 'validate'
    * sont initialisées ici car on utilise les fonctions d'internationalisation __()
    * que l'on ne peut pas utiliser lors de la déclaration de la variable
    *
    * @param  array  $options [description]
    * @return [type]          [description]
    */
    public function beforeValidate($options = [])
    {
        // nom : unique et obligatoire
        $this->validate['nom']['obligatoire'] = [
            'rule' => 'notBlank',
            'required' => true,
            'message' => __('Information obligatoire, veuillez saisir un nom', true)
        ];
        $this->validate['nom']['unique'] = [
            'rule' => 'isUnique',
            'message' => __('Ce nom est deja utilise, veuillez saisir un autre nom')
        ];
        // actif : au moins un actif
        $this->validate['actif']['auMoinsUnActif'] = [
            'rule' => 'verifActif',
            'message' => __('Il faut au moins un circuit de traitement actif, veuillez cocher ce circuit actif.')
        ];


        return true;
    }

    public function beforeSave($options = [])
    {
        if (empty($this->data[$this->alias]['id'])) {
            $this->data[$this->alias]['created_user_id'] = AuthComponent::user('id');
        }
        $this->data[$this->alias]['modified_user_id'] = AuthComponent::user('id');

        return true;
    }

    /**
     * Vérifie qu'au moins un circuit est actif
     */
    public function verifActif()
    {
        return (
                $this->data[$this->name]['actif'] ||
                $this->find('count', [
                    'conditions' => [
                        'Circuit.actif' => true],
                    'recursive' => -1]));
    }

    /**
     * Détermine si une instance peut être supprimée tout en respectant l'intégrité référentielle
     * Paramètre : id
     */
    public function isDeletable($id)
    {
        // Existence de l'instance en base
        if (!$this->find('count', ['conditions' => ['id' => $id], 'recursive' => -1])) {
            return false;
        }

        // Existence d'un traitement
        if ($this->Traitement->find('count', ['conditions' => ['circuit_id' => $id], 'recursive' => -1])) {
            return false;
        }

        return true;
    }

    public function beforeFind($query = [])
    {
        $query['conditions'] = (is_array($query['conditions'])) ? $query['conditions'] : [];
        $db = $this->getDataSource();

        //Gestion des droits sur les types d'actes
        //  'allow' => array()
        // if (&& !isset($query['Deliberation.typeacte_id']))
        //if (!empty($query['allow']) && in_array( $query['allow'], 'Deliberation.typeacte_id'))
        if (!empty($query['allow']) && in_array('id', $query['allow'], true)) {
            $Aro = ClassRegistry::init('Aro');

            $Aro->Behaviors->attach('Database.DatabaseTable');
            $Aro->Permission->Behaviors->attach('Database.DatabaseTable');
            $Aro->Permission->Aco->Behaviors->attach('Database.DatabaseTable');

            $subQuery = [
                'fields' => [
                    'Aco.foreign_key'
                ],
                'joins' => [
                    $Aro->join('Permission', ['type' => 'INNER']),
                    $Aro->Permission->join('Aco', ['type' => 'INNER'])
                ],
                'conditions' => [
                    'Aro.foreign_key' => AuthComponent::user('id'),
                    'Permission._read' => 1,
                    'Aco.model' => 'Circuit'
                ]
            ];

            $subQuery = $Aro->sql($subQuery);

            $subQuery = ' "' . $this->alias . '"."id" IN (' . $subQuery . ') ';

            $subQueryExpression = $db->expression($subQuery);
            $conditions[] = $subQueryExpression;

            $query['conditions'] = array_merge($query['conditions'], $conditions);
        }

        return $query;
    }

    /**
     * Mise à jour du champ défaut
     */
    public function afterSave($created, $options = [])
    {
        if (Configure::read('Cakeflow.manage.default') && $this->data[$this->name]['defaut']) {
            $this->updateAll(
                ['Circuit.defaut' => 'false'],
                ['Circuit.id <>' => $this->id]
            );
        }
    }

    /**
     * Retourne l'id du circuit actif par défaut
     * @return integer id du circuit actif par défaut
     */
    public function getDefautId()
    {
        $data = $this->find('first', [
            'conditions' => ['actif' => true, 'defaut' => true],
            'fields' => ['id'],
            'recursive' => -1]);
        if (empty($data)) {
            return null;
        } else {
            return $data['Circuit']['id'];
        }
    }

    /**
     * Retourne les id des circuits actifs et ayant au moins une étape
     * @return array liste des id
     */
    public function actifsAuMoinsUneEtape()
    {
        // initialisations
        $ret = null;
        // lecture de tous les circuits actifs
        $circuits = $this->find('all', [
            'conditions' => ['actif' => true],
            'fields' => ['id'],
            'recursive' => -1]);
        foreach ($circuits as $circuit) {
            if ($this->Etape->hasAny(['circuit_id' => $circuit['Circuit']['id']])) {
                $ret[] = $circuit['Circuit']['id'];
            }
        }

        return $ret;
    }

    public function getList()
    {
        return $this->find('list', ['conditions' => ['actif' => 1],
                    'order' => ['nom']]);
    }

    public function listeCircuitsParUtilisateur($user_id)
    {
        $circuits = [];
        $compositions = $this->Etape->Composition->find('all', ['conditions' => ['Composition.trigger_id' => $user_id]]);
        foreach ($compositions as $composition) {
            if (!empty($composition['Etape']['circuit_id'])) {
                array_push($circuits, $composition['Etape']['circuit_id']);
            }
        }
        return implode($circuits, ',');
    }

    public function getLibelle($circuit_id = null)
    {
        if ($circuit_id == null) {
            return '';
        }

        $circuit = $this->find('first', [
            'field' => ['id', 'nom'],
            'conditions' => ['Circuit.id' => $circuit_id],
            'recursive' => -1,
        ]);

        return $circuit['Circuit']['nom'];
    }

    public function userInCircuit($user_id, $circuit_id)
    {
        $etapes = $this->Etape->find('all', ['conditions' => ['Etape.circuit_id' => $circuit_id]]);
        foreach ($etapes as $etape) {
            foreach ($etape['Composition'] as $composition) {
                if ($composition['trigger_id'] == $user_id) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Insert une cible ($targetId) dans le circuit de traitement $circuitId
     * @param integer $circuitId identifiant du circuit
     * @param integer $targetId identifiant de la cible (objet à faire traiter dans le circuit)
     * @param integer $createdUserId idenifiant de l'utilisateur connecté a l'origine de la création
     * @return bool
     */
    public function insertDansCircuit($circuitId, $targetId, $createdUserId = null)
    {
        // On vérifie l'existence du circuit
        if (!$this->find('count', ['conditions' => ['id' => $circuitId], 'recursive' => -1])) {
            return false;
        }
        // On vérifie l'existence de la cible
        if (!$this->Traitement->{CAKEFLOW_TARGET_MODEL}->find('count', ['conditions' => ['id' => $targetId], 'recursive' => -1])) {
            return false;
        }
        // On vérifie que le traitement n'existe pas déjà en base
        if ($this->Traitement->find('count', ['conditions' => ['target_id' => $targetId], 'recursive' => -1])) {
            return false;
        }

        // Ajout d'une occurence dans la table traitement
        $this->Traitement->create();
        $traitement = ['Traitement'=> [
            'circuit_id' => $circuitId,
            'target_id' => $targetId,
            'numero_traitement' => 1,
            'treated' => false,
            'created_user_id' => $createdUserId,
            'modified_user_id' => $createdUserId,

        ]];
        $this->Traitement->save($traitement);
        $newTraitementId = $this->Traitement->id;

        // Ajout des occurences dans la table visa
        $etapes = $this->Etape->find('all', [
            'fields' => ['Etape.id', 'Etape.nom', 'Etape.type', 'Etape.ordre', 'Etape.cpt_retard'],
            'contain' => ['Composition.trigger_id', 'Composition.type_validation'],
            'conditions' => ['Etape.circuit_id' => $circuitId],
            'order' => ['Etape.ordre ASC'],
            'recursive' => -1
        ]);

        foreach ($etapes as $etape) {
            foreach ($etape['Composition'] as $composition) {
                $this->Traitement->Visa->create();
                $visa = ['Visa' => [
                    'traitement_id' => $newTraitementId,
                    'trigger_id' => $composition['trigger_id'],
                    'signature_id' => 0,
                    'etape_nom' => $etape['Etape']['nom'],
                    'etape_id' => $etape['Etape']['id'],
                    'etape_type' => $etape['Etape']['type'],
                    'date_retard' => $this->Etape->computeDateRetard($etape['Etape']['cpt_retard'], $targetId),
                    'action' => 'RI',
                    'commentaire' => null,
                    'date' => null,
                    'numero_traitement' => $etape['Etape']['ordre'],
                    'type_validation' => $composition['type_validation'],
                ]];
                $this->Traitement->Visa->save($visa);
                $this->Traitement->Visa->clear();
            }
        }

        return $newTraitementId;
    }

    /**
     * Ajoute au traitement terminé de la cible $targetId les étapes du circuit $circuitId
     * @param integer $circuitId identifiant du circuit
     * @param integer $targetId identifiant de la cible (objet à faire traiter dans le circuit)
     * @param integer $createdUserId idenifiant de l'utilisateur connecté a l'origine de la création
     * @return bool
     */
    public function ajouteCircuit($circuitId, $targetId, $createdUserId = null)
    {
        // on vérifie l'existence du circuit
        if (!$this->find('count', ['recursive' => -1, 'conditions' => ['id' => $circuitId]])) {
            return false;
        }
        // on vérifie l'existence de la cible
        if (!$this->Traitement->{CAKEFLOW_TARGET_MODEL}->find('count', ['recursive' => -1, 'conditions' => ['id' => $targetId]])) {
            return false;
        }
        // on vérifie que le traitement existe
        if (!$this->Traitement->find('count', ['recursive' => -1, 'conditions' => ['target_id' => $targetId]])) {
            return false;
        }

        // lecture du traitement
        $traitement = $this->Traitement->find('first', [
            'recursive' => -1,
            'fields' => ['id', 'numero_traitement'],
            'conditions' => [
                'target_id' => $targetId,
                'treated' => true]]);
        if (empty($traitement)) {
            return false;
        }

        // ajout des occurences dans la table visa
        $etapes = $this->Etape->find('all', [
            'fields' => ['Etape.id', 'Etape.nom', 'Etape.type', 'Etape.ordre', 'Etape.cpt_retard'],
            'contain' => ['Composition.trigger_id', 'Composition.type_validation'],
            'conditions' => ['Etape.circuit_id' => $circuitId],
            'order' => ['Etape.ordre ASC'],
            'recursive' => -1
        ]);
        // count($users) > 1 ? 2 : 1,
        // trigger_id' => $users,
        foreach ($etapes as $etape) {
            foreach ($etape['Composition'] as $composition) {
                $visa = $this->Traitement->Visa->create();
                $visa['Visa']['traitement_id'] = $traitement['Traitement']['id'];
                $visa['Visa']['trigger_id'] = $composition['trigger_id'];
                $visa['Visa']['signature_id'] = 0;
                $visa['Visa']['etape_nom'] = $etape['Etape']['nom'];
                $visa['Visa']['etape_id'] = $etape['Etape']['id'];
                $visa['Visa']['etape_type'] = $etape['Etape']['type'];
                $visa['Visa']['date_retard'] = $this->Etape->computeDateRetard($etape['Etape']['cpt_retard'], $targetId);
                $visa['Visa']['action'] = 'RI';
                $visa['Visa']['commentaire'] = '';
                $visa['Visa']['date'] = null;
                $visa['Visa']['numero_traitement'] = $traitement['Traitement']['numero_traitement'] + $etape['Etape']['ordre'];
                $visa['Visa']['type_validation'] = $composition['type_validation'];
                $this->Traitement->Visa->save($visa);// vérifie si l'on a un ou plusieur utilisateur pour la meme étape
            }
        }

        // mise à jour du traitement
        $traitement['Traitement']['circuit_id'] = $circuitId;
        $traitement['Traitement']['modified_user_id'] = $createdUserId;
        $traitement['Traitement']['treated'] = false;
        $traitement['Traitement']['numero_traitement'] ++;
        $this->Traitement->save($traitement);

        return true;
    }

    /**
     * [hasEtapeDelegation description]
     * @param  int  $id circuit_id
     * @return boolean
     */
    public function hasEtapeDelegation($id)
    {
        $etapes = $this->Etape->find('all', [
            'conditions' => [
                'Etape.circuit_id' => $id
            ],
            'contain' => ['Composition.type_validation'],
        ]);
        $compositions = Hash::extract($etapes, '{n}.Composition.{n}.type_validation');
        return in_array('D', $compositions, true);
    }

    public function parentNode()
    {
        return null;
    }

    public function parentNodeAlias()
    {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        $data = $this->data;
        if (empty($this->data)) {
            $data = $this->read();
        }

        return ['Circuit' => ['alias' => $data['Circuit']['nom']]];
    }

    /**
     * Duplique un projet en rinitialisant certain champs, pour que la délibération
     * soit comme nouvellement créé
     *
     * @version 5.3
     * @access public
     * @param type $id --> id du projet à copier
     */
    public function duplicate($id)
    {
        $circuit = $this->find('first', [
            'contain' => [
              'Etape'=>['Composition']
            ],
            'conditions' => ['Circuit.id' => $id],
            'recursive' => -1
        ]);

        $circuit['Circuit']['nom'] = __('%s - copie', $circuit['Circuit']['nom']);
        $circuit['Circuit']['actif'] = false;
        foreach ($circuit['Etape'] as &$etape) {
            foreach ($etape['Composition'] as &$composition) {
                $composition['modified_user_id'] = $composition['created_user_id'] = AuthComponent::user('id');
                unset($composition['id'], $composition['created'], $composition['modified']);
            }
            $etape['modified_user_id'] = $etape['created_user_id'] = AuthComponent::user('id');
            unset($etape['id'], $etape['circuit_id']);
        }
        unset($circuit['Circuit']['id'], $circuit['Circuit']['created_user_id'], $circuit['Circuit']['modified_user_id'], $circuit['Circuit']['created'], $circuit['Circuit']['modified'], $circuit['Etape']['id']);
        $this->create();
        unset($this->Etape->validate['circuit_id']);
        unset($this->Etape->Composition->validate['type_composition']);
        return $this->saveAssociated($circuit, ['deep' => true]);
    }
}
