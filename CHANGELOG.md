# Change Log

Toutes les modifications apportées au projet seront documentées dans ce fichier.

Le format est basé sur le modèle [Keep a Changelog](http://keepachangelog.com/) et adhère aux principes du [Semantic Versioning](http://semver.org/).

## [7.0.0] - 2024-01-04

### Evolutions
- Changement de la license open source vers MIT
- Prise en charge de la diffusion avec packagist

## [6.0.0] - 2022-12-12

### Evolutions
- Prise en charge de php en version 7.4
- Prise en charge du parapheur FAST sans visa

## [5.4.4] - 2022-09-05

### Corrections
- Modification de circuit

## [5.4.3] - 2022-07-07

### Corrections
- Récupération d'une annulation de délégation Pastell

## [5.4.2] - 2022-06-21

### Corrections
- vérification de l'état d'une délégation Pastell avec l'état du dossier au lieu de savoir s'il y a une signature


## [5.4.1] - 2022-06-14

### Corrections
- L'ajout de composition lors d'une étape de circuit la délégation de validation avec un flux Pastell studio n'était pas pris en compte

## [5.4.0] - 2022-05-13

### Evolutions
- Prendre en compte lors d'une composition d'étape de circuit la délégation de validation avec un flux Pastell studio
 
## [5.3.2] - 2022-05-06

### Corrections
- Affiche les circuits de signature pastell alors que cela n'est pas possible avec la délégation

## [5.3.1] - 2020-10-12

### Corrections
- Correction mise à jour sql de la version 5.3

## [5.3] - 2020-04-23

### Evolutions
- Prise en compte du Multi-rédacteur pour l'utilisateur générique "Rédacteur du projet" dans une composition
- Migration vers Font Awesome 5

### Correction
-  Création d'un index sur les visas

## [5.2.3] - 2019-07-17

### Correction
- Pas de pagination dans les listes d'étapes et de compositions des circuits

## [5.2.2] - 2019-06-25

### Correction
- Dernière action d'un utilisateur dans une étape collaborative ne prend pas le dernier valideur

## [5.2.1] - 2019-02-22

### Evolutions
- Saisir plus d'une seule valeur par critère dans le formulaire de filtre

### Correction
- Génération du schéma de base pour validation

## [5.2.0] - 2019-01-31

### Ajouts
- Dupliquer les circuits de validations

### Evolutions
- Suppressions des circuits seulement si ceux-ci ne contiennent pas de traitements

### Corrections
- Gestion du retour lors de la suppression de toutes les étapes d'un circuit
- Lorsqu'un projet est refusé dans IP, le commentaire de refus apparaissait en double
- Mise en place de la visibilité des fonctions suivant la norme PSR

## [5.1.0] - 2017-07-04

### Ajouts
- Ajout d'une fonction de récupération de l'étape courante d'un traitement
- Liste les utilisateurs du circuit en rapport avec un visa

### Evolutions
- Commentaire de refus et de validation lors d'une délégation de circuit iparapheur

### Corrections
- Les dates de retard ne s'initialisaient pas


## [5.0.1] - 2017-04-13

### Corrections
- Envoi des mails lors des refus dans les circuits avec délégations

## [5.0.0] - 2017-03-19

### Evolutions
- Prise en charge de php en version 7

### Corrections
- Prise en compte de l'optimisation pour l'insertion dans un circuit
- Alertes de retard qui n'étaient pas initialisé avec la date

## [4.1.0] - 2016-07-15

### Ajouts
- Mise en place des connecteurs de délégation

### Evolutions
- Modifications des circuits par un utilisateur de type manager

### Corrections
- Prise en compte de l'optimisation pour l'insertion dans un circuit

### Suppressions
- Suppression des transactions des script sql

## [4.0.0]

### Evolutions
- Mise en place de bootstrap pour l'affichage des composants html

## [3.1.2]

### Evolutions
- Ajout d'une étape construite à la volée pour l'action "Envoyer à"

### Corrections
- Correction getLastVisaTrigger dernier visa effectué dans un circuit

## [3.1.1]

### Evolutions
- Suppression de la contrainte de base de donnée sur wkf_visas

### Corrections
- Correction du retour lors de l'ajout d'une composition

## [3.1.0]

### Ajouts
- Ajout de la fonctionnalité de réglage du retard pour la validation des étapes.
- Délégation de validation vers parapheur désormais générique. Usage de PASTELL indisponible.
### Evolutions
- La création d'un circuit est désormais plus rapide.

## [3.0.2]

### Evolutions
- Nouveau paramétrage sur l'ajout d'un traitement "IN"
- Prendre en compte "IN" comme "RI" dans positionTrigger

### Corrections
- listeSousTypesParapheur pour le parapheur
- Lien entre les models Traitement et Visa
- Fonction "retourner à" : ne recrée que les étapes précédentes et non plus tout le circuit précédent

## [3.0.1]

### Evolutions
- Remise de l'attribut état parapheur à 0 après récupération du dossier

### Corrections
- Saut d'étape automatique (validation des visas)
- Récupération de validation parapheur (délégation)

## [3.0.0]

### Ajouts
- Mise en place d'une étape de type délégation

### Corrections
- Création d'étape de circuit
- Saut d'étape automatique

[5.2.3]: https://gitlab.libriciel.fr/CakePHP/Cakeflow/compare/5.2.2...5.2.3
[5.2.2]: https://gitlab.libriciel.fr/CakePHP/Cakeflow/compare/5.2.1...5.2.2
[5.2.1]: https://gitlab.libriciel.fr/CakePHP/Cakeflow/compare/5.2.0...5.2.1
[5.2.0]: https://gitlab.libriciel.fr/CakePHP/Cakeflow/compare/5.1.0...5.2.0
[5.1.0]: https://gitlab.libriciel.fr/CakePHP/Cakeflow/compare/5.0.1...5.1.0
[5.0.1]: https://gitlab.libriciel.fr/CakePHP/Cakeflow/compare/5.0.0...5.0.1
[5.0.0]: https://gitlab.libriciel.fr/CakePHP/Cakeflow/compare/4.1.0...5.0.0
[4.1.0]: https://gitlab.libriciel.fr/CakePHP/Cakeflow/tags/4.1.0
