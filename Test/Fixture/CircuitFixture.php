<?php

/**
 * Code source de la classe CircuitFixture.
 *
 * PHP 5.3
 *
 * @package app.Test.Fixture
 * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
 */

/**
 * Classe CircuitFixture.
 *
 * @package Cakeflow.Test.Fixture
 */
class CircuitFixture extends CakeTestFixture {

    /**
     * On importe la définition de la table et les enregistrements.
     *
     * @var array
     */
    public $import = array('model' => 'Cakeflow.Circuit', 'records' => true, 'connection' => 'test_fixture');

}