<?php

class VisaFixture extends CakeTestFixture {
    /**
     * On importe la définition de la table et les enregistrements.
     *
     * @var array
     */
    public $import = array('model' => 'Cakeflow.Visa', 'records' => true, 'connection' => 'test_fixture');
}